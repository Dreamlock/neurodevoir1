#Authors: Victor Fan & Philippe Spino
#CIP: fanv2501 & spip2401
from brian2 import *
import matplotlib.pyplot as plt
import numpy as np

delay = 50.5*2
highFreq = 1*2
lowFreq = 5*2

eqsNeurons = '''
dv/dt = (I)/(tau) : 1
I : 1
tau : second
'''

#Defining Groups of Neurons
walkingGroup = NeuronGroup(4, eqsNeurons, threshold='v >= 1', reset='v = 0', method='euler')
walkingGroup.I = np.ones(4)
walkingGroup.tau = [highFreq, lowFreq, highFreq, lowFreq]*ms

upToggleGroup = NeuronGroup(2, eqsNeurons, threshold='v >= 1', reset='v = 0', method='euler')
upToggleGroup.I = np.ones(2)
upToggleGroup.tau = [delay+0.2, delay+0.2]*ms

legGroup = NeuronGroup(6, eqsNeurons, threshold='v > 0.95', reset='v = 0', method='euler')
legGroup.I = np.zeros(6)
legGroup.tau = np.ones(6)*ms

directionCaptorsGroup = NeuronGroup(5, eqsNeurons, threshold='v >= 1', reset='v = 0', method='euler')
directionCaptorsGroup.I = np.zeros(5)
directionCaptorsGroup.tau = [1, 1, 10, highFreq/2, lowFreq/2]*ms

#Controls
#LeftCaptor connected to the activation of the right legs.
leftCaptor = Synapses(directionCaptorsGroup,walkingGroup,on_pre='v_post = 0')
leftCaptor.connect(i = 0, j = [2, 3])
leftCaptor2Ups = Synapses(directionCaptorsGroup,upToggleGroup,on_pre='v_post = 0')
leftCaptor2Ups.connect(i = 0, j = [0, 1])
leftCaptor2Out = Synapses(directionCaptorsGroup,legGroup,on_pre='v_post = 0')
leftCaptor2Out.connect(i = 0, j = 3)

#rightCaptor connected to the activation of the left legs.
rightCaptor = Synapses(directionCaptorsGroup,walkingGroup,on_pre='v_post = 0')
rightCaptor.connect(i = 1, j = [0, 1])
rightCaptor2Ups = Synapses(directionCaptorsGroup,upToggleGroup,on_pre='v_post = 0')
rightCaptor2Ups.connect(i = 1, j = [0, 1])
rightCaptor2Out = Synapses(directionCaptorsGroup,legGroup,on_pre='v_post = 0')
rightCaptor2Out.connect(i = 1, j = 0)

#frontCaptor connected to the activation of the position of the legs.
frontCaptor = Synapses(directionCaptorsGroup,upToggleGroup, on_pre='v_post = 1')
frontCaptor.connect(i = 2, j = 0)
frontCaptor.connect(i = 2, j = 1)

#backCaptor connected to the activation of the acceleration of the legs.
backCaptorBack = Synapses(directionCaptorsGroup, walkingGroup, on_pre='v_post += 0.5')
backCaptorBack.connect(i = 3, j = [0, 2])
backCaptorFront = Synapses(directionCaptorsGroup, walkingGroup, on_pre='v_post += 0.5')
backCaptorFront.connect(i = 4, j = [1, 3])

#Left side
walkBackOut1 = Synapses(walkingGroup, legGroup, on_pre='v_post += 0.2')
walkBackOut1.connect(i = 0, j = 0)

walkFrontOut1 = Synapses(walkingGroup, legGroup, on_pre='v_post += 0.2')
walkFrontOut1.connect(i = 1, j = 0)

upOut1 = Synapses(upToggleGroup, legGroup, on_pre='v_post = 1')
upOut1.connect(i = 0, j = 0)

inhibitorWalkBackSelf1 = Synapses(walkingGroup, walkingGroup, on_pre='v_post = -0.1', delay=delay*ms)
inhibitorWalkBackSelf1.connect(i = 0, j = 0)

inhibitorWalkBackWalkFront1 = Synapses(walkingGroup, walkingGroup, on_pre='v_post = 0')
inhibitorWalkBackWalkFront1.connect(i = 0, j = 1)

propagationLeg1Leg21 = Synapses(legGroup, legGroup, on_pre='v_post = 1', delay=delay*ms)
propagationLeg1Leg21.connect(i = 0, j = 1)

propagationLeg2Leg31 = Synapses(legGroup, legGroup, on_pre='v_post = 1', delay=delay*ms)
propagationLeg2Leg31.connect(i = 1, j = 2)

#Right side
walkBackOut2 = Synapses(walkingGroup, legGroup, on_pre='v_post += 0.2')
walkBackOut2.connect(i = 2, j = 3)

walkFrontOut2 = Synapses(walkingGroup, legGroup, on_pre='v_post += 0.2')
walkFrontOut2.connect(i = 3, j = 3)

upOut2 = Synapses(upToggleGroup, legGroup, on_pre='v_post = 1')
upOut2.connect(i = 1, j = 3)

inhibitorWalkBackSelf2 = Synapses(walkingGroup, walkingGroup, on_pre='v_post = -0.1', delay=delay*ms)
inhibitorWalkBackSelf2.connect(i = 2, j = 2)

inhibitorWalkBackWalkFront2 = Synapses(walkingGroup, walkingGroup, on_pre='v_post = 0')
inhibitorWalkBackWalkFront2.connect(i = 2, j = 3)

propagationLeg1Leg22 = Synapses(legGroup, legGroup, on_pre='v_post = 1', delay=delay*ms)
propagationLeg1Leg22.connect(i = 3, j = 4)

propagationLeg2Leg32 = Synapses(legGroup, legGroup, on_pre='v_post = 1', delay=delay*ms)
propagationLeg2Leg32.connect(i = 4, j = 5)

Out = StateMonitor(legGroup, 'v', record=True)
Walk = StateMonitor(walkingGroup, 'v', record=True)
Up = StateMonitor(upToggleGroup, 'v', record=True)
Controls = StateMonitor(directionCaptorsGroup,'v', record=True)

########################Start of the Scenarios########################
run(200*ms)

#Scenario 1: Detecting something behind
directionCaptorsGroup.I = [0,0,0,1,1]
run(500*ms)

#Scenario 2: Stop walking
directionCaptorsGroup.I = np.zeros(5)
walkingGroup.I = np.zeros(4)
upToggleGroup.I = np.zeros(2)
run(50*ms)

#Scenario 3: Detecting something in front
directionCaptorsGroup.I = [0,0,1,0,0]
walkingGroup.I = np.ones(4)
upToggleGroup.I = np.ones(2)
run(10*ms)

directionCaptorsGroup.I = np.zeros(5)
run(500*ms)

#Scenario 4: Detecting something on the left side
directionCaptorsGroup.I = [1,0,0,0,0]
run(500*ms)

#Scenario 5: Detecting something on the right side
directionCaptorsGroup.I = [0,1,0,0,0]
run(500*ms)

#Walk the rest of the way
directionCaptorsGroup.I = [0,0,0,0,0]
run(500*ms)

########################End of the Scenarios########################

#Plots for test scenarios
fig, axs = plt.subplots(3,2)
plt.figure(num=1)
axs[0][0].plot(Out.t/ms, Out.v[0], label='Out 1')
axs[1][0].plot(Out.t/ms, Out.v[1], label='Out 2')
axs[2][0].plot(Out.t/ms, Out.v[2], label='Out 3')
axs[0][1].plot(Out.t/ms, Out.v[3], label='Out 4')
axs[1][1].plot(Out.t/ms, Out.v[4], label='Out 5')
axs[2][1].plot(Out.t/ms, Out.v[5], label='Out 6')

#Example of leg output zoomed in
plt.figure(num=2)
plot(Out.t/ms, Out.v[3], label='Out 1')

#Plots for captors
plt.figure(num=3)
plot(Controls.t/ms,Controls.v[0],label='captor left')
plot(Controls.t/ms,Controls.v[1],label='captor right')
plot(Controls.t/ms,Controls.v[2],label='captor front')
plot(Controls.t/ms,Controls.v[3],label='captor back Back')
plot(Controls.t/ms,Controls.v[4],label='captor back Front')

#Plots of walk controller
plt.figure(num=4)
plot(Walk.t/ms, Walk.v[0], label='Walk 1')
plot(Walk.t/ms, Walk.v[1], label='Walk 2')

#Plots of position controller
plt.figure(num=5)
plot(Up.t/ms, Up.v[0], label='Up')

plt.xlabel('Time (ms)')
plt.ylabel('v')
plt.legend()

plt.show()